'use strict';

const mode = 'development';
// const mode = 'production';

const configServer = {
    notify: false,
    server: {
        baseDir: "./"
    }
};
const path = {
    build: {
        html: './',
        js: 'js/',
        css: 'css/',
        img: 'img/',
        fonts: 'fonts/'
    },
    src: {
        html: 'src/*.html',
        js: 'src/js/app.js',
        css: 'src/scss/styles.scss',
        img: 'src/img/**/*',
        fonts: 'src/fonts/**/*'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        css: 'src/scss/**/*.scss',
        img: 'src/img/**/*',
        fonts: 'srs/fonts/**/*'
    },
    clean: 'js/, css/, img/, fonts/'
};

const gulp = require('gulp');
const webpack = require('webpack-stream');
const browsersync = require('browser-sync');
const plumber = require('gulp-plumber');
const rigger = require('gulp-rigger');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const del = require('del');

const browserSync = done => {
    browsersync.init(configServer);
    done();
};

const browserSyncReload = done => {
    browsersync.reload();
    done();
};

const buildHtml = () => {
    return gulp
        .src(path.src.html)
        .pipe(rigger())
        .pipe(plumber())
        .pipe(gulp.dest(path.build.html))
        .pipe(browsersync.stream());
};

const buildCss = () => {
    return gulp
        .src(path.src.css)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(cleanCSS())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(path.build.css))
        .pipe(browsersync.stream());
};

const buildJs = () => {
    return gulp
        .src(path.src.js)
        .pipe(webpack({
            mode: mode,
            output: {
                filename: 'scripts.js'
            },
            watch: false,
            devtool: 'source-map',
            module: {
                rules: [{
                    test: /\.m?js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                ['@babel/preset-env', {
                                    debug: true,
                                    corejs: 3,
                                    useBuiltIns: 'usage'
                                }],
                                '@babel/react'
                            ]
                        }
                    }
                }]
            }
        }))
        .pipe(gulp.dest(path.build.js))
        .pipe(browsersync.stream());
};

const buildImages = () => {
    return gulp
        .src(path.src.img)
        .pipe(
            imagemin([
                imagemin.gifsicle({interlaced: true}),
                imagemin.jpegtran({progressive: true}),
                imagemin.optipng({optimizationLevel: 5}),
                imagemin.svgo({
                    plugins: [
                        {
                            removeViewBox: false,
                            collapseGroups: true
                        }
                    ]
                })
            ])
        )
        .pipe(gulp.dest(path.build.img))
        .pipe(browsersync.stream());
};

const buildFonts = () => {
    return gulp
        .src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
        .pipe(browsersync.stream());
};

const buildClean = () => del([path.clean]);

const watchFiles = () => {
    gulp.watch(path.watch.html, gulp.series(buildHtml, browserSyncReload));
    gulp.watch(path.watch.css, gulp.series(buildCss, browserSyncReload));
    gulp.watch(path.watch.js, gulp.series(buildJs, browserSyncReload));
    gulp.watch(path.watch.img, gulp.series(buildImages, browserSyncReload));
    gulp.watch(path.watch.fonts, gulp.series(buildFonts, browserSyncReload));
};

const build = () => gulp.series(
    buildClean,
    gulp.parallel(buildHtml, buildCss, buildJs, buildImages, buildFonts)
);

exports.build = build();
exports.default = gulp.parallel(build(), watchFiles, browserSync);