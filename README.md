## A BLANK PROJECT FOR WORK

### GENERAL INFO:
1. The toolkit: **Gulp**, **Webpack**.
2. The preprocessors: **SASS**, **BABEL**.
3. Frameworks: **none**.
4. The JS library: **jQuery**.
5. Support: **ES6**.

### HOW TO USE IT:
1. Open a terminal and input: `npm i --save-dev`.
2. Run the task: `gulp`.
3. Make all files change in the folder: **src/**.
4. Use **https://realfavicongenerator.net/** to generate favicons and move them to the folder: **favicons/**.
5. Use **https://icomoon.io/** to generate a web font from svg icons.

#### - HTML:
- Edit a home page in the file: **src/index.html**.
- Use the **gulp-rigger** for html files.
- Layout and the connections are in files: **src/layouts/**.

### - SCSS:
- Make all changes in the files: **src/scss/**.
- Main variables are in the files: **src/scss/_custom/**.
- Read comments in the files: **styles.scss**, **_config.scss** and **_custom-config.scss**.
- All plugins should be added to **src/scss/_libs/**.

### - JS:
- Add imports and initializations in the file: **src/js/app.js**.
- All function should be added to **src/js/functions/**.
- All plugins should be added to **src/js/libs/**.

### - FONTS
- All fonts should be added to **src/fonts/**.

### - IMAGES
- All images should be added to **src/img/**.