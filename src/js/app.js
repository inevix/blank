'use strict';

import afterLoad from './functions/after-load';

document.addEventListener('DOMContentLoaded', () => {
    afterLoad();
});